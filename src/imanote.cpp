/****************************************************************************
 **
 ** Copyright (C) Alexis.
 **
 ** This "sticky note" application is derived from Nokia's ShapedClock
 ** and therefore published under LGPL license as explained below
 **
 ****************************************************************************/

/****************************************************************************
 **
 ** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
 ** All rights reserved.
 ** Contact: Nokia Corporation (qt-info@nokia.com)
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:LGPL$
 ** Commercial Usage
 ** Licensees holding valid Qt Commercial licenses may use this file in
 ** accordance with the Qt Commercial License Agreement provided with the
 ** Software or, alternatively, in accordance with the terms contained in
 ** a written agreement between you and Nokia.
 **
 ** GNU Lesser General Public License Usage
 ** Alternatively, this file may be used under the terms of the GNU Lesser
 ** General Public License version 2.1 as published by the Free Software
 ** Foundation and appearing in the file LICENSE.LGPL included in the
 ** packaging of this file.  Please review the following information to
 ** ensure the GNU Lesser General Public License version 2.1 requirements
 ** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 **
 ** In addition, as a special exception, Nokia gives you certain additional
 ** rights.  These rights are described in the Nokia Qt LGPL Exception
 ** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
 **
 ** GNU General Public License Usage
 ** Alternatively, this file may be used under the terms of the GNU
 ** General Public License version 3.0 as published by the Free Software
 ** Foundation and appearing in the file LICENSE.GPL included in the
 ** packaging of this file.  Please review the following information to
 ** ensure the GNU General Public License version 3.0 requirements will be
 ** met: http://www.gnu.org/copyleft/gpl.html.
 **
 ** If you have questions regarding the use of this file, please contact
 ** Nokia at qt-info@nokia.com.
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

 #include "imanote.h"

 ImaNote::ImaNote(QWidget *parent)
     : QWidget(parent, Qt::FramelessWindowHint ),
     isResizing(false), mTextEdit(this), mBackGroundColor(255,255,127), mSticon(this)
 {
     setWindowIcon(QIcon(QPixmap("/usr/share/icons/imanote.png")));
     setWindowIconText("I'm a Note");

     mSticon.setIcon(QIcon(QPixmap("/usr/share/icons/imanote.png"))); // On assigne une image à notre icône
     mSticon.show(); // On affiche l'icône

     setAttribute(Qt::WA_TranslucentBackground);

     QAction *quitAction = new QAction(tr("E&xit"), this);
     quitAction->setShortcut(tr("Ctrl+Q"));
     connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
     addAction(quitAction);

     setContextMenuPolicy(Qt::ActionsContextMenu);
     setToolTip(tr("Drag the note with the left mouse button.\n"
                   "Use the right mouse button to open a context menu."));
     //setWindowTitle(tr("I'm a Note"));

     QPalette p(palette());
     // Set background colour to light yellow
     p.setColor(QPalette::Base, mBackGroundColor);
     setPalette(p);

     //text editor :
     mTextEdit.setFrameShape( QTextEdit::NoFrame );
     mTextEdit.setFrameShadow( QTextEdit::Plain );
     mTextEdit.setAutoFillBackground (true);
     mTextEdit.setPalette(p);
     mTextEdit.setTextColor(QColor(32,0,96));//dark blue
     mTextEdit.setFont(QFont("Purisa",16,QFont::DemiBold));
     mTextEdit.setFontPointSize(16);
     mTextEdit.show();

     //TODO create file in home directory to save data

     // put the note at desktop center
     move(QApplication::desktop()->geometry().center() - rect().center());

     // track mouse position for resizing (in right-bottom corner)
     setMouseTracking(true);
 }

 void ImaNote::mousePressEvent(QMouseEvent *event)
 {
     int resizearea = 32;
     int xlim, ylim;

     if (event->button() == Qt::LeftButton) {
         xlim = width()-resizearea;
         ylim = (int) height()-resizearea;
         if (event->x() > xlim && event->y() > ylim) {
            setCursor(Qt::SizeFDiagCursor);
            QPoint tmp = event->globalPos() - frameGeometry().topLeft();
            dragSize = QSize(tmp.x(),tmp.y());
            isResizing = true;
            event->accept();
         } else {
             setCursor(Qt::ArrowCursor);
             dragPosition = event->globalPos() - frameGeometry().topLeft();
             event->accept();
         }
      }
 }

 void ImaNote::mouseReleaseEvent(QMouseEvent *)
 {
     isResizing = false;
 }

 void ImaNote::mouseMoveEvent(QMouseEvent *event)
 {
     int resizearea = 32;
     int xlim, ylim;

     xlim = width()-resizearea;
     ylim = (int) height()-resizearea;

     if (event->x() > xlim && event->y() > ylim) {
        setCursor(Qt::SizeFDiagCursor);
     } else {
        setCursor(Qt::ArrowCursor);
     }

     if (isResizing) {
         setCursor(Qt::SizeFDiagCursor);
         QPoint tmp = event->globalPos() - frameGeometry().topLeft();
         dragSize = QSize(tmp.x(),tmp.y());
         resize(dragSize);
         event->accept();
     } else {
         if (event->buttons() & Qt::LeftButton) {
             move(event->globalPos() - dragPosition);
             event->accept();
         }
     }
 }

 void ImaNote::paintEvent(QPaintEvent *)
 {
     /*static const QPoint hourHand[3] = {
         QPoint(7, 8),
         QPoint(-7, 8),
         QPoint(0, -40)
     };
     static const QPoint minuteHand[3] = {
         QPoint(7, 8),
         QPoint(-7, 8),
         QPoint(0, -70)
     };

     QColor hourColor(127, 0, 127);
     QColor minuteColor(0, 127, 127, 191);

     int side = qMin(width(), height());
     QTime time = QTime::currentTime();

     QPainter painter(this);
     painter.setRenderHint(QPainter::Antialiasing);
     painter.translate(width() / 2, height() / 2);
     painter.scale(side / 200.0, side / 200.0);

     painter.setPen(Qt::NoPen);
     painter.setBrush(hourColor);

     painter.save();
     painter.rotate(30.0 * ((time.hour() + time.minute() / 60.0)));
     painter.drawConvexPolygon(hourHand, 3);
     painter.restore();

     painter.setPen(hourColor);

     for (int i = 0; i < 12; ++i) {
         painter.drawLine(88, 0, 96, 0);
         painter.rotate(30.0);
     }

     painter.setPen(Qt::NoPen);
     painter.setBrush(minuteColor);

     painter.save();
     painter.rotate(6.0 * (time.minute() + time.second() / 60.0));
     painter.drawConvexPolygon(minuteHand, 3);
     painter.restore();

     painter.setPen(minuteColor);

     for (int j = 0; j < 60; ++j) {
         if ((j % 5) != 0)
             painter.drawLine(92, 0, 96, 0);
         painter.rotate(6.0);
     }*/

     QPainterPath path;
     int padding = 2;

     path = getNotePath(padding);
     //TODO : translation/rotation of path (needs Qt >= 4.6)

     //QPixmap pixmap("/home/alexis/.icons/postit.png");
     //pixmap = pixmap.scaled(width(),height());
     QPainter painter(this);
     painter.translate(padding,padding);     

     //painter.drawPixmap(0,0,pixmap);

     painter.setRenderHint(QPainter::Antialiasing);
     painter.setPen(Qt::NoPen);
     painter.setBrush(mBackGroundColor);
     //painter.translate(1, 1);
     painter.drawPath(path);

 }

 void ImaNote::resizeEvent(QResizeEvent * /* event */)
 {
     QPixmap pixmap(width(),height());
     pixmap.fill(Qt::transparent);

     QPainterPath path;
     int padding = 0;

     path = getNotePath(padding);
     path.boundingRect();

     QPainter painter(&pixmap);
     painter.translate(padding,padding);

     painter.setPen(Qt::NoPen);
     painter.setBrush(mBackGroundColor);
     painter.drawPath(path);

     setMask(pixmap.mask());

     int offset = 25;
     mTextEdit.setGeometry(offset,offset,width()-2*offset,height()-2*offset);
 }

 QSize ImaNote::sizeHint() const
 {
     return QSize(320, 320);
 }

 QPainterPath ImaNote::getNotePath(int padding)
 {
     QPainterPath path;

     int w = width()-padding*2;
     int h = height()-padding*2;
     path.cubicTo(0.03f*w,0.20f*h,0.0325f*w,0.40f*h,0.0275f*w,0.6f*h);
     path.cubicTo(0.025f*w,0.70f*h,0.010f*w,0.85f*h,0.005f*w,0.988f*h);
     path.cubicTo(0.07f*w,0.99f*h,0.14f*w,0.992f*h,0.22f*w,0.994f*h);
     path.cubicTo(0.33f*w,0.992f*h,0.44f*w,0.99f*h,0.557f*w,0.988f*h);
     path.cubicTo(0.7f*w,0.982f*h,0.85f*w,0.976f*h,0.9948f*w,0.97f*h);
     path.cubicTo(0.951f*w,0.75f*h,0.951f*w,0.5f*h,0.951f*w,0.3588f*h);
     path.cubicTo(0.9495f*w,0.2f*h,0.9479f*w,0.1f*h,0.9479f*w,0.006f*h);
     path.closeSubpath();

     return path;
 }
