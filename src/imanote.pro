HEADERS       = imanote.h
 SOURCES       = imanote.cpp \
                 main.cpp

 # install
 target.path = $$[QT_INSTALL_EXAMPLES]/widgets/imanote
 sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS imanote.pro
 sources.path = $$[QT_INSTALL_EXAMPLES]/widgets/imanote
 INSTALLS += target sources

 symbian {
     TARGET.UID3 = 0xA000C605
     include($$QT_SOURCE_TREE/examples/symbianpkgrules.pri)
 }
